#include "bifrost_2d.h"

#if BIFROST_OS_WIN32
#include <windows.h>
#endif

using namespace bf;

struct player
{
    bf2d::texture2d Texture;
    vec3 Position;
    float Rotation;
};

struct game_state
{
    bool Initialized;

    float TimerInSeconds;
    player Player;
};

#if BIFROST_OS_WIN32
int WinMain(HINSTANCE Instance, HINSTANCE PrevInstance, PSTR CmdLine, INT CmdShow)
#elif BIFROST_OS_LINUX
int main()
#endif
{
    bf2d::InitializeWindow(800, 600, "demo game");

    bf2d::camera mainCamera = bf2d::CreateOrthographicCamera(vec2(-4, -3), vec2(4, 3));
    bf2d::SetCamera(mainCamera);

    game_state gameState = {};
    gameState.Player.Texture = bf2d::LoadTexture("debug-texture.png");
    gameState.Player.Rotation = 0;
    gameState.Player.Position = vec3(0.0f, 0.0f, 1.0f);

    gameState.TimerInSeconds = 0.0f;
    gameState.Initialized = 1;

    float accumulator = 0.0f;

    float targetTicksPerSecond = 150;
    float targetSecondsPerTick = 1.0f / targetTicksPerSecond;
    float secondsPerTick = targetSecondsPerTick;

    // NOTE(jacob): Main game loop
    while(bf2d::IsRunning())
    {
        bf2d::BeginFrame();

        float frameTimeInSeconds = bf2d::GetFrameTime();

        accumulator += frameTimeInSeconds;
        while(accumulator >= secondsPerTick)
        {
            accumulator -= secondsPerTick;
            // NOTE(jacob): Tick game simulation forward 1 tick
            gameState.Player.Rotation += secondsPerTick * 90.0f;
        }

        bf2d::ClearScreen(vec4(0, 0, 0, 1));

        bf2d::FillRect(vec2(1), vec2(0.2f), 0, vec4(0, 0, 1, 1));
        bf2d::FillSprite(vec2(0), vec2(1), gameState.Player.Rotation, gameState.Player.Texture);

        bf2d::EndFrame();
    }

    bf2d::CleanupWindow();
    return 0;
}

