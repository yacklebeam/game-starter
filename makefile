CXX=clang++
CFLAGS=-DDEBUG -D_REENTRANT -DGLFW_DLL -DBIFROST_RENDERER -w -g

ifeq ($(OS), Windows_NT)
	FILES=./code/game.cpp ./bifrost/src/bifrost_2d.cpp
	LIBS=-lglew32 -lglfw3dll -lopengl32
	LIBPATH=-L"./bifrost/lib/win32"
	INCLUDES=-I"./bifrost/include" -I"./bifrost/include/win32" -I"./bifrost/src"
	OUTPUT_FILE=build/game.exe

	MAKE_BUILD=if not exist build mkdir build
	MAKE_RELEASE=if not exist release mkdir release
	DEL_BUILD=del .\\build\\* /Q >NUL
	DEL_RELEASE=del .\\release\\* /Q >NUL

	PACK_ASSETS=copy .\\assets\\* .\\release >NUL
	PACK_DLLS=copy .\\bifrost\\lib\\*.dll .\\release >NUL
	PACK_EXE=copy .\\build\\game.exe .\\release >NUL
else
	FILES=./code/game.cpp ./bifrost/src/bifrost_2d.cpp
	LIBS=-lGL -lGLEW -lglfw
	LIBPATH=
	INCLUDES=-I"./bifrost/include" -I"./bifrost/src"
	OUTPUT_FILE=build/game

	DEL_BUILD=rm -f build/*
	DEL_RELEASE=rm -f release/*

	PACK_ASSETS=cp assets/* release/
	PACK_DLLS=cp ./bifrost/lib/*.dll release/
	PACK_EXE=cp build/game release/
endif

game:
	$(CXX) $(INCLUDES) -o $(OUTPUT_FILE) $(CFLAGS) $(FILES) $(LIBPATH) $(LIBS)

clean:
	$(MAKE_BUILD)
	$(MAKE_RELEASE)
	$(DEL_BUILD)
	$(DEL_RELEASE)

package:
	$(PACK_ASSETS)
	$(PACK_DLLS)
	$(PACK_EXE)

all: clean game package ;
