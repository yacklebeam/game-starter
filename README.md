# Game Jam Starter

- Linux + Win32 platform layers
- OpenGL w/ Glew + GLFW

## About

This project was born from my frustration at having to do basic stuff every time I wanted to start a game jam -- this codebase is my starting point for 99% of my game jam projects (2D graphics, OpenGL, etc).

I DO NOT RECOMMEND anyone else use it, there is a fair bit of weirdness built in based on how I like to do things...

The intention of this project is NOT to provide a game-engine like experience.  While working on a jam game, I fully expect to have to work on the renderer, the platform, etc -- NOT JUST GAME CODE.
However, this project provides a nice starting point for the basics, so that I can start getting into game code right away.

## Getting Started

Clone this code into a new repo -- game code lives in game.cpp.

See build_win32.bat and build_linux.sh for examples of how to build this project (I use the so-called "unity" build, not to be confused with the game engine).

Make sure necessary includes and libraries are in place and the build files are looking for them correctly.

I would advise adding the build scripts to your .gitignore, to allow different machines to have different build scripts.

## Prerequisites

- Neovim, preferred editor (needs v0.5 or higher)
- clangd, preferred c LSP
- clang(++), preferred compiler
- make, preferred make system (or just use shell scripts)

On Windows, all of these can be installed via chocolatey (packages: neovim -pre, llvm, make)
